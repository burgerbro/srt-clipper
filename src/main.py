import re
import os
import ffmpy
import srt
import datetime
import argparse
import glob


class Detection:

    def __init__(self, srt_file, sentence, detected_word, dt_start, dt_end):
        self.srt_file = srt_file
        self.sentence = sentence
        self.detected_word = detected_word
        self.dt_start = dt_start
        self.dt_end = dt_end

    def __str__(self, ):
        return f"{self.srt_file.split('/')[-1]} - {self.sentence} [{self.detected_word}] @ {self.dt_start}"

class Clipper:

    def __init__(self,video_filepath,srt_filepath,detector,output_name="output.webm",interval_buffer = datetime.timedelta(seconds=15),clip_length=datetime.timedelta(seconds=30)):
        self.video_filepath = video_filepath
        self.srt_filepath = srt_filepath
        self.detector = detector
        self.output_name = output_name
        self.interval_buffer = interval_buffer
        self.clip_length = clip_length

    def __str__(self):
        return "Clipper for {}".format(self.filepath)

    def do_clips(self,output_name=None,audio=False,subtitles=True):
        if output_name is None:
            output_name = self.output_name
        outputs = []
        for (scene_no,scene) in enumerate(self.detector):
            scene_output_name = output_name.replace(".webm","_{}.webm".format(scene_no+1))
            worker = ffmpy.FFmpeg(
                    inputs={self.video_filepath: None},
                    outputs = {scene_output_name: "-crf 4 -sn -vf scale=640:-1 -c:v libvpx -ss {} -t {} {} {}".format(scene.dt_start - self.interval_buffer,self.clip_length,(subtitles) * "-vf subtitles='{}'".format(self.srt_filepath),(not audio) * "-an")}
                    #outputs = {scene_output_name: "-ss {} -t 00:00:30.000".format(scene.dt_start-self.interval_buffer)}
                    )
            outputs +=[scene_output_name]
            print(worker.cmd)
            worker.run()
        input_dict = {}
        for output in outputs:
            input_dict[output] = None
        filter_complex_string = "{}concat=n={}:v=1:a={}[outv]{} -map '[outv]'".format("".join(["[{}:v:0]".format(i) + (audio)*"[{}:a:0]".format(i) for i,_ in enumerate(outputs)]),len(outputs),(audio)*1,(audio) * "[outa] -map'[outa]'")
        worker = ffmpy.FFmpeg(
                inputs=input_dict,
                outputs={'output.webm':"-crf 4 -sn -c:v libvpx -filter_complex {}".format(filter_complex_string)}
                )
        #ffmpeg -i output_1.webm -i output_2.webm -filter_complex "[0:v:0][1:v:0]concat=n=2:v=1:a=0[outv]" -map "[outv]" -sn -c:v libvpx -crf 4  output.webm
        worker.run()
            

def clean_up(text):
    return text.replace("!", "") \
        .replace(".", "") \
        .replace(",", "") \
        .replace("?", "") \
        .lower()


def parse_keywords(keywords_path):
    with open(keywords_path, 'r') as f:
        keywords_set = {x.lower().rstrip() for x in f.readlines()}
    return keywords_set


def detect_keywords(text_, keyword):
    m = re.search(f"(^|\s){keyword}($|\s)", text_)
    if m:
        return True
    return False


def parse_srt(srt_file, keywords_set):
    detected_keywords = []
    #srt_files = [f"{subtitles_folder}/{x}" for x in os.listdir(subtitles_folder) if x.endswith(".srt")]

    print(f"Detecting keywords in: {srt_file}")
    with open(srt_file, "r") as f:
        for sentence in srt.parse(f.read()):
            text = sentence.content
            text_ = clean_up(text)

            for keyword in keywords_set:
                if detect_keywords(text_, keyword):
                    detection = Detection(srt_file, text, keyword, sentence.start, sentence.end)
                    detected_keywords.append(detection)

    return detected_keywords


def main():
    parser = argparse.ArgumentParser(description="Input the name of a folder in media to isolate propaganda identified by key words")
    parser.add_argument('folder',metavar='f',type=str,help='the location of the folder containing both the .srt and .mp4/.mkv')
    args = parser.parse_args()
    folder_location = args.folder
    #subtitles_folder = "subtitles"
    srt_file = glob.glob("{}/*.srt".format(folder_location)).pop()
    keywords_path = "keywords.txt"
    keywords_set = parse_keywords(keywords_path)
    detected_list = parse_srt(srt_file, keywords_set)
    for detected in detected_list:
        print("[{}] @ {}\n\t\"{}\"".format(detected.detected_word,detected.dt_start,detected.sentence.replace("\n","")))
    input()
    types = ('*.mkv','*.mp4')
    video_files = []
    for file_type in types:
        video_files.extend(glob.glob("{}/*.{}".format(folder_location,file_type)))
    if len(video_files) == 0:
        print("No videos found. Terminating")
    else:
        video_file = video_files.pop()
        clipper = Clipper(video_file,srt_file,detected_list)
        clipper.do_clips(audio=False)


if __name__ == '__main__':
    main()
